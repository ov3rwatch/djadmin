-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.49-1ubuntu8.1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema dj_admin
--

CREATE DATABASE IF NOT EXISTS dj_admin;
USE dj_admin;
CREATE TABLE  `dj_admin`.`clients` (
  `idcli` int(11) NOT NULL AUTO_INCREMENT,
  `namecli` char(250) NOT NULL,
  `statuscli` char(30) NOT NULL,
  PRIMARY KEY (`idcli`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
CREATE TABLE  `dj_admin`.`events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client` char(150) NOT NULL,
  `eventname` char(150) NOT NULL,
  `description` char(150) DEFAULT '-',
  `date` char(10) NOT NULL,
  `location` char(130) NOT NULL,
  `address1` char(150) NOT NULL,
  `town` char(100) NOT NULL,
  `zip` int(5) NOT NULL,
  `timestart` char(12) NOT NULL,
  `timeend` char(12) NOT NULL,
  `eventtype` char(60) NOT NULL,
  `soldier` char(1) NOT NULL DEFAULT '-',
  `attendees` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
CREATE TABLE  `dj_admin`.`users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL,
  `password` char(250) NOT NULL,
  `type` char(20) NOT NULL,
  `fname` char(30) NOT NULL,
  `lname` char(35) NOT NULL,
  `email` char(35) NOT NULL,
  `client` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
INSERT INTO `dj_admin`.`users` VALUES  (34,'admin','ceb07f1b8b1e081c6fe166d240d9d5313035e66c','admin','Admin','User','admin@djadmin.tld','Your Company Name Here');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
