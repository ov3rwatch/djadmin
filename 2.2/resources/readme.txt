READ ME for DJ Admin PA2
+++++++++++++
Software: DJ Admin
Version: PA2 [PreAlpha2]
Developer: JL Griffin
SourceForge User: griffinwebnet
+++++++++++++
NOTE:make sure you set up the database first, and that you have set the dj_admin directory chmod -R 777 
OR THE INSTALL WILL FAIL!!!

Note: i havent built a database setup script yet so i have included a sql file that you can import into your server.

Its set to CREATE a database called dj_admin. if your host doesnt allow creation of NEW databses, simply change every instance of:
dj_admin with your database's name or the name you wish to make you database.

Sorry for the inconvenience :(

+++++++++++++
Credits:

> 7Loops (Theme Assistance) [Infinity Theme]
> Stefan Gabos (Dialog boxes) [Zebra Dialog]
> Jordan Boesch (Dialog boxes) [Gritter]
> DFC Engineering (Form styling)[JQTransform]
> loktar (Location List) [JCRUMB]
> Andreas Eberhard (IE PNG Transparency) [JQPNGfix.js]

There are a handfull of others, but i cant think of them at the moment (I'm only human right? :D )

======================================Change Log from Version PA1 to PA2==========================

+Added documentation to the install system as well as the check login system.

+*Added a footer template [footer.php]
	>>Defines copyright line to your company name defined in the setup installer.
	>>Javascript defines current year to define copyright year

+Fixed JQTransform bug which wasnt styling checkboxes, radio buttons, numeric updown controls

+Fixed installer bug which caused an error due to a missing \" 

+Fixed Buffer Overflow Bug in php2 & 3

+Added an output buffer for use with php 4.7.3+ to checklogin.php

+Added a buffer flush for use with php 4.7.3+ to checklogin.php

+*Removed CGE Branding.
	>> Added a custom font to handle the logo
	>> logo reflects your company name

+*Removed login dialog branding
	>> added a loghead.php to make login dialog reflect your company name
	
+*Added a title.php to make the window title reflect your company name	

[* = Created by the install system]

=============================Security Change Log from Version PA1 to PA2==========================

+Added Password Hashing for security
 >> sha1 is the hashing i used because its more secure. however in future versions I will double and triple hash it with several encryptions (Ex: sha1, md5 etc etc)
 note: md5 can be used but you would need change a few lines that handle hashing in several files:
 	- Line 55 in changepass.php
 	- Line 20 in insertusr.php
 	- Line 13 in checklogin.php
 	- Line 06 in /install/dbsetup.php
 ^^ these would need to be changed PRIOR to running website.com/install. otherwise the user you set up there will be unable to log in as its password will be hashed in sha1 and not md5
 Also if you already have several users setup, drop the users table, recreate from the sql file and start over as none of the previously made users will be able to log in.
 In future versions you will be able to simply reset all users to a hashed version of password01 in whatever hashing youd like to use (which is how future hashing system 
 updates will be applied)
 
 If you want to use md5 instead, you can save database bandwidth by changing the password files in the database to CHAR(75) instead of CHAR(150)
 
==================================================================================================