<?php
session_start();
if(!session_is_registered("myusername")){
header("location:./login.php");
}
if(session_is_registered("client")){
	header("location: ./dashboard.php");
}
?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
     <title><?php include('title.php'); ?></title>
    <meta name="description" content="">
    <meta name="author" content="JL Griffin" >
    
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed|Ubuntu' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
    
    <!-- CSS: implied media=all -->
    <link rel="stylesheet" href="css/table.css">
    <link rel="stylesheet" href="css/fullcalendar.css">
    <link rel="stylesheet" href="css/simplemodal.css">
    <link rel="stylesheet" href="css/jquery.gritter.css">
    <link rel="stylesheet" href="css/jquery.wysiwyg.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css">
    <link rel="stylesheet" href="css/elfinder.min.css">
    <link rel="stylesheet" href="css/jqtransform.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- end CSS-->
    <link href='http://fonts.googleapis.com/css?family=Economica:700' rel='stylesheet' type='text/css'>
    
    <!-- CSS Media Queries for Standard Devices -->
    <!--[if !IE]><!-->
        <link rel="stylesheet" href="css/devices/smartphone.css" media="only screen and (min-width : 320px) and (max-width : 767px)">
        <link rel="stylesheet" href="css/devices/ipad.css" media="only screen and (min-width : 768px) and (max-width : 1024px)"> 
    <!--<![endif]-->
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
        
    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
         Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
         For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="js/libs/modernizr-2.0.6.min.js"></script>
</head>

<body>
    <div id="body-container">
        <div id="container">
	        <header>
	            <a id="logo">DJ Admin | <?php echo $_SESSION['coname']; ?></a>
	            <div id="main-navigation">
	                	<ul>
	                  <li><a href="./dashboard.php#?=overview_pane?refid=<?php echo $_SESSION['username']; ?>" id="dashboard-m"><span class="dashboard-32" title="Dashboard area">Dashboard</span></a></li>
	                  <?php if(session_is_registered("staff")){
	                  echo "<li><a href='./admin.php#?=adm_auth_key=2985679588431' class='active' id='elements-m'><span class='files-32' title='Admin Area'>Admin Area</span></a></li>";} ?>
	                  <?php if(session_is_registered("admin")){
	                  echo "<li><a href='./admin.php#?=adm_auth_key=2985679588431' class='active' id='elements-m'><span class='files-32' title='Admin Area'>Admin Area</span></a></li>";} ?>
	                  </ul>
	            </div> 
	            <div id="profile">
	                <div id="user-data">Welcome: <?php echo $_SESSION["fname"]; ?> <?php echo $_SESSION["lname"]; ?><br />User: <?php echo $_SESSION["username"]; ?></div>
                <div class="clearfix"></div>
                <div id="user-notifications">
                    <ul>
                        <li><a class="settings-16 tt-top-center" href="changelogin.php" title="Change your Password"></a></li>
                        <li><a class="profile-16 tt-top-center" title="Change your Settings"></a></li>                        
                        <li><a href="./logout.php" id="logout" class="logout-16 tt-top-center" title="Click here to logout">Logout</a></li>
                    </ul>                 
	                    <div class="clearfix"></div>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="clearfix"></div>
	        </header>
	        <div id="main" role="main">
	            <div id="sub-navigation">
	                <div id="navigation-search">
	                    <form>
	                        <input type="text" name="search" id="search" placeholder="Search"></input>
	                    </form>
	                </div>
	                
                
                <ul>
                    <li><a title="Song Requests" class="tt-top-center"><?php echo $_SESSION["reqcount"]; ?></a><span>Requests</span></li>
                    <li><a class="blue tt-top-center" title="Number of Upcoming events"><?php echo $_SESSION["eventcount"]; ?></a><span>Upcoming Events</span></li>
                    <li><a class="green tt-top-center" title="Number of completed events"><?php echo $_SESSION["ceventcount"]; ?></a><span>Completed Events</span></li>
                </ul>
	                
	                <a class="comment-16 tt-top-center" title="Send a message to CG Entertainment" id="show-modal">Send Us A Message</a>
	            <!--     <a class="info-16 tt-top-center" title="Show global notifications" id="add-notify">notifications</a> -->
	                <div class="clearfix"></div>
	            </div>
	        
	            <div id="main-container">
	            
	                <!--! you can put content from elements.html, forms.html, file.html and charts.html below here to see other sections -->
	                
	                <!--! dashboard -->
	                <div id="body-section">
					    <div id="left-menu">
					        <ul>
					            <li class="menu-trigger"><a href="./admin.php#?adm_auth_key=2985679588431" class="data-16" id="c-data">View Events</a></li>
					            <li class="menu-trigger"><a href="./add-events.php#?adm_auth_key=2985679588431" class="data-16" id="c-data">Add Events</a></li>
					            <li class="menu-trigger"><a href="./view-users.php#?adm_auth_key=2985679588431" class="profile-16" id="profile-16">View Users</a></li>
					            <li class="menu-trigger active"><a href="./add-users.php#?adm_auth_key=2985679588431" class="profile-16" id="profile-16">Add Users</a></li>
					            <li class="menu-trigger"><a href="./view-clients.php#?adm_auth_key=2985679588431" class="users-16" id="profile-16">View Clients</a></li>
					            <li class="menu-trigger"><a href="./add-clients.php#?adm_auth_key=2985679588431" class="users-16" id="profile-16">Add Clients</a></li>
					        </ul>
					        <div class="clearfix"></div>
					    </div>
					    <div id="content-container">
					        <div id="content">
					            <div class="c-overview">
					                <div class="bredcrumbs">
					                    <ul>
					                        <li><a href="#">User: <?php echo $_SESSION["username"]; ?></a></li>
					                        <li><a href="#">Administration</a></li>
					                        <li><a href="#">Add Users</a></li>
					                    </ul>
					                    <div class="clearfix"></div>
					                </div>

					          
					            </div>
					        
					                     <div class="box-element">
					                    <div class="box-head">Add User</div>
					                    <div class="box-content no-padding grey-bg">
					                        					                    <div id="forms" class="tab-content no-padding">
					                        <form method="post" action="./insertusr.php" class="i-validate"> 
					                            <fieldset>
					                                <section>
					                                    <div class="section-left-s">
					                                        <label for="text_field">Username</label>
					                                    </div>                                  
					                                    <div class="section-right">
					                                        <div class="section-input"><input type="text" placeholder="Enter New Username" name="username" id="username" class="i-text required"></input></div>
					                                    </div>
					                                    <div class="clearfix"></div>
					                                </section>                          
					                                <section>
					                                    <div class="section-left-s">
					                                       <label for="text_field">First Name</label>
					                                    </div>                                  
					                                    <div class="section-right">
					                                       <div class="section-input"><input type="text" placeholder="User's First Name" name="fname" id="fname" class="i-text required"></input></div>
					                                    </div>
					                                    <div class="clearfix"></div>
					                                </section>
					                                <section>
					                                    <div class="section-left-s">
					                                       <label for="text_field">Last Name</label>
					                                    </div>                                  
					                                    <div class="section-right">
					                                       <div class="section-input"><input type="text" placeholder="User's Last Name" name="lname" id="lname" class="i-text required"></input></div>
					                                    </div>
					                                    <div class="clearfix"></div>
					                                </section> 
					                                <section>
					                                    <div class="section-left-s">
					                                       <label for="text_tooltip">E-Mail</label>
					                                    </div>                                  
					                                    <div class="section-right">                                         
					                                       <div class="section-input"><input type="text" placeholder="User's Email Address" name="email" id="email" class="i-text required" title="Email"></input></div>
					                                    </div>
					                                    <div class="clearfix"></div>
					                                </section> 
					                                <section>
					                                    <div class="section-left-s">
					                                    <label for="select">Account Type</label> 
					                                    </div>                                  
					                                    <div class="section-right">                                        
					                                        <div class="section-input i-transform">
					                                            <select id="actype" name="actype">
					                                              <option value="client" selected="true">Client</option> 
					                                              <option value="staff">Staff</option> 
					                                              <option value="admin">Administrator</option> 
					                                              </select>
					                                        </div>
					                                    </div>
					                                    <div class="clearfix"></div>
					                                </section>
					                                <section>
					                                    <div class="section-left-s">
					                                    <label for="select">Client</label> 
					                                    </div>                                  
					                                    <div class="section-right">                                        
					                                        <div class="section-input i-transform">
					                                            <select id="client" name="client">
					                                             <?php

														$con = mysql_connect($_SESSION["dbhost"] ,$_SESSION["dbuser"] ,$_SESSION["dbpass"]);
														if (!$con)
 														 {
														  die('Could not connect: ' . mysql_error());
														  }

														mysql_select_db($_SESSION["dbname"], $con);

														$result = mysql_query("SELECT * FROM clients");

                            while ($row = mysql_fetch_assoc($result)) {
                                echo '<option value="' . $row['namecli'] . '">' . $row['namecli'] . '</option>';
                            
                         }
                            ?>

					                                              </select>
					                                        </div>
					                                    </div>
					                                    <div class="clearfix"></div>
					                                </section>         
					                            </fieldset>
					                        </form>
					                    </div> 
					                        <div class="clearfix"></div>
					                    </div>
					                </div>
					            					                        
					            	</div>
					                    </div>
					                
					    <div class="clearfix"></div>
					</div> <!--! end of dashboard -->
					
					
	            </div> <!--! end of #main-container -->
	        </div>
	        <footer>
		          <?php
	             include('footer.php');
		     ?>
	       </footer>
	    </div> <!--! end of #container -->
    
    </div> <!--! end of #body-container -->
    
    <!-- modal content -->
    <div id="modal-content" class="modal-container">
        <div class="modal-head"><h3>Send Us A Message</h3></div>
        <div class="modal-body">
                <input value="<?php echo $_SESSION['username']; ?>" type="hidden">
                <div class="i-label">
                    <label for="text_field">Subject:</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input"><input type="text" name="msgsubj" id="msgsubj" class="i-text required"></input></div>
                </div>
                <div class="i-divider"></div>
                <div class="i-label">
                    <label for="text_field">Message</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input"><textarea rows="10" name="msgabout" id="msgabout" class="i-text required"></textarea></input></div>
                </div>
                <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            <input type="submit" name="submit" id="" class="i-button no-margin" value="Submit" />
            <div class="clearfix"></div>
        </div>
    </div>
     
    <!-- JavaScript at the bottom for fast page loading -->
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/mylibs/excanvas.min.js"></script><![endif]-->
    <script language="javascript" type="text/javascript" src="js/libs/jquery-1.6.2.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/libs/jquery-ui-1.8.16.custom.min.js"></script>
    
    <!-- scripts -->
    <script language="javascript" type="text/javascript" src="js/mylibs/elfinder.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.flot.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.flot.pie.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.flot.resize.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.flot.stack.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.flot.crosshair.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.dataTables.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.tools.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/fullcalendar.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.gritter.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.simplemodal.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.autogrowtextarea.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.wysiwyg.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/controls/wysiwyg.image.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/controls/wysiwyg.link.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/controls/wysiwyg.table.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.idTabs.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.validate.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/chosen.jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.jqtransform.js"></script>
    <script language="javascript" type="text/javascript" src="js/mylibs/jquery.ba-hashchange.min.js"></script>
    <script defer src="js/init.js"></script>
    <script defer src="js/bootstrap.js"></script>
    <script type="text/javascript" >
	$('.i-transform').jqTransform();
	</script>
    <link rel="Stylesheet" type="text/css" href="./css/zebra_dialog.css">
    <script type="text/javascript" src="./js/zebra_dialog.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
	
			$.Zebra_Dialog('<strong>User Has Not Been Addded!</strong> <br><hr> There was an error processing your request! <br>Please fill out the form again, and retry.', {
    		'type':     'error',
    		'title':    'Error!'
			});
		});
	</script>
    <!-- end scripts-->
    
    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
         chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->
  
</body>
</html>
