<?php
//Start the Output buffer.
//to make sure output strings are kept in the buffer
ob_start();
//start session to begin Variable Declaration...
session_start();
// Configuration Information
include('config.php');
// username and password sent from form
$myusername    =  $_POST['myusername'];
$unhashedpass  =  $_POST['mypassword'];
//Hash the password for security
$mypassword    =  sha1($unhashedpass);
// To protect from MySQL injection attack
$myusername    =  stripslashes($myusername);
$mypassword    =  stripslashes($mypassword);
//to make sure Termination Charactors like ['],["],[\] dont end the strings before theyre supposed to.
//For more info on this visit [http://codepointsolutions.com/docs/msql+php/escape_strings/]
$myusername    =  mysql_real_escape_string($myusername);
$mypassword    =  mysql_real_escape_string($mypassword);
//Select ALL rows froom the users table in the database & make sure the user exists and that
//the correct password has been supplied... [Set up the query variable]
$sql="SELECT * FROM $tbl_name WHERE username='$myusername' and password='$mypassword'";
//Run the query we just set up...
$result = mysql_query($sql);
//if the user exists, select all the info we need from that user...
while($row = mysql_fetch_array($result))
  {
// <removed because of a bug>  $school= $row['school'];
  $actype= $row['type'];
  $fname=  $row['fname'];
  $lname=  $row['lname'];
  $cli=  $row['client'];
  }
  ;

// Mysql_num_row is counting table row
$count=mysql_num_rows($result);
// If result matched $myusername and $mypassword, table row must be 1 row
if($count==1){
// Register $myusername, $mypassword and redirect to file "dashboard.php" as 
//well as set up all needed variables
$_SESSION["username"]      = $myusername;
$_SESSION["password"]      = $mypassword;
// <caused a bug> $_SESSION["schid"]  = $school;
$_SESSION["actype"]        = $actype;
$_SESSION["fname"]         = $fname;
$_SESSION["lname"]         = $lname;
$_SESSION["cli"]           = $cli;

$_SESSION["dbhost"]        = $host;
$_SESSION["dbuser"]        = $username;
$_SESSION["dbpass"]        = $password;
$_SESSION["dbname"]        = $db_name;

$_SESSION["reqcount"]      = "&#8734;";
$_SESSION["eventcount"]    = "&#8734;";
$_SESSION["ceventcount"]   = "&#8734;";
$_SESSION["coname"]        = $coname;

session_register($actype); // <-- register as staff, admin, ior client depending...
session_register("myusername"); // <-- for page validation
session_register("mypassword"); // <-- For page validation

//If user uses the default Password [password01], direct them to the password change page.
if($unhashedpass == "password01"){
header("location: changelogin.php");	
	}
	else {
		//if everything is ok, send them to their unique dashboard:
header("location: dashboard.php#?=_pane?refid=" . $myusername . "_" . $actype);
}
//If there is an error in the login, bad user/ bad password/ cant connect to db redirect to the error page.
}
else {
header("location: login2.php");
// <removed from an old version> echo "The Username or Password you Have entered is incorrect.";
}
//flush [empty] the output buffer and exit;
ob_flush();
?>